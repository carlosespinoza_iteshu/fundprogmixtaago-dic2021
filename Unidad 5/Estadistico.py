nombreArchivo=input("Nombre del archivo a leer: ")
try:
    archivo=open(nombreArchivo,"r")
except:
    print("Archivo no existe...")
    exit()
facturas=archivo.readlines()
archivo.close()
montoMayor=0
cliente=""
totalImporte=0
totalIva=0
total=0
for factura in facturas:
    datos=factura.split(",")
    totalImporte+=float(datos[2])
    totalIva+=float(datos[3])
    total+=float(datos[4])
    if(float(datos[4])>montoMayor):
        montoMayor=float(datos[4])
        cliente=datos[1]
print("El total de importe es: $", totalImporte)
print("El total de IVA es: $", totalIva)
print("El total es: $", round(total,2))
print("La factura mayor es por: $", montoMayor, " y es del cliente: ", cliente)
