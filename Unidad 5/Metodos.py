#Realiza un programa que simule un cajero automatico, al inciar debera mostrar 
#un menú con las opciones de: Crear cuenta, Retirar Efectivo, depositar y Salir; 
# la cuenta deberá tener. El nombre del usuario y un password, toma en cuenta que no se podrá
# retirar mas dinero que el que esta en la cuenta.

listaUsuarios=[]
usuario=""
password=""

def Menu():
    print("\tMenú del cajero automatico")
    print("1. Crear una cuenta.")
    print("2. Retirar efectivo")
    print("3. Depositar efectivo")
    print("4. Salir")
    opcion=input("\tOpcion deseada:")
    return opcion

def CrearCuenta():
    listaUsuarios.append(dict(
        Nombre=input("Nombre del usuario: "),
        Password=input("Teclea tu contraseña: "),
        Saldo=0
    ))

def ValidaUsuario(usuario, password):
    for user in listaUsuarios:
        if usuario==user["Nombre"] and password==user["Password"]:
            return True
    return False

def Depositar(usuario, monto):
    for user in listaUsuarios:
        if user["Nombre"]==usuario:
            user["Saldo"]+=monto
            print("Ahora tu saldo es de: $",user["Saldo"])

def Retirar(usuario, monto):
    for user in listaUsuarios:
        if user["Nombre"]==usuario:
            if user["Saldo"]>=monto:
                user["Saldo"]=user["Saldo"]-monto
                print("Dinero retirado")
            else:
                print("No hay saldo suficiente")

def SolicitaDatosDelUsuario():
    usuario=input("Nombre de usuario:")
    password=input("Contraseña:")

while True:
    opcion=Menu()
    if opcion=="1":
        CrearCuenta()
    if opcion=="2":
        SolicitaDatosDelUsuario()
        if ValidaUsuario(usuario,password):
            monto=float(input("Monto a retirar:$"))
            Retirar(usuario,monto)
        else:
            print("Usuario y/o contraseña incorrecta")
    if opcion=="3":
        SolicitaDatosDelUsuario()
        if ValidaUsuario(usuario,password):
            monto=float(input("Monto a depositar:$"))
            Depositar(usuario,monto)
        else:
            print("Usuario y/o contraseña incorrecta")
    if opcion=="4":
        print("Adios!!!")
        break #Termina el programa, al salir del While
    #print(listaUsuarios)
