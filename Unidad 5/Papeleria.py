import os
from datetime import datetime

def AgregarProducto():
    try:
        productos=open("Productos.dat","a")
        codigo=input("Código del producto: ")
        nombre=input("Nombre del producto:")
        precio=float(input("Precio del producto: $"))
        datos=codigo + "|" + nombre + "|" + str(precio) + "\n"
        productos.write(datos)
        productos.close()
        print("==>Producto agregado :)")
    except:
        print("Error al abrir el archivo")
   

def ListarProductos():
    productos=open("Productos.dat","r")
    datos=productos.readlines()
    productos.close()
    print("Código\tNombre\tPrecio")
    for producto in datos:
        print(producto.replace("|","\t"))
    
def EditarProducto(): 
    try: 
        codigo=input("Código del producto a editar: ")
        nombre=input("Nuevo nombre: ")
        precio=float(input("Precio: $"))
        archOrigen=open("Productos.dat","r")
        archTemp=open("temp.dat","w")
        while(True):
            linea=archOrigen.readline()
            if not linea:
                break
            datos=linea.split("|")
            if(datos[0]==codigo):
                archTemp.write(codigo + "|" + nombre + "|" + str(precio) + "\n")
            else:
                archTemp.write(linea)
        archTemp.close()
        archOrigen.close()
        os.remove("Productos.dat")
        os.rename("temp.dat","Productos.dat")
    except:
        print("Error al manejar los archivos")

def EliminarProducto():
    try: 
        codigo=input("Código del producto a eliminar: ")
        archOrigen=open("Productos.dat","r")
        archTemp=open("temp.dat","w")
        while(True):
            linea=archOrigen.readline()
            if not linea:
                break
            datos=linea.split("|")
            if(datos[0]!=codigo):
                archTemp.write(linea)
        archTemp.close()
        archOrigen.close()
        os.remove("Productos.dat")
        os.rename("temp.dat","Productos.dat")
    except:
        print("Error al manejar los archivos")

def MenuProductos():
    while(True):
        print("\n\n\tMenú de productos")
        print("\n1. Agregar productos")
        print("2. Editar producto")
        print("3. Eliminar producto")
        print("4. Listar productos")
        print("9. Regresar...")
        opcion=int(input("\tOpcion: "))
        if(opcion==1):
            AgregarProducto()
        if(opcion==2):
            EditarProducto()
        if(opcion==3):
            EliminarProducto()
        if(opcion==4):
            ListarProductos()
        if(opcion==9):
            break
        

def BuscarProducto(codigo, productos):
    for producto in productos:
        datos=producto.split("|")
        if(datos[0]==codigo):
            return producto.replace("\n","").split("|")
    return None
        


def NuevaVenta():
    cliente=input("Nombre de cliente: ")
    fecha=datetime.now()
    archivo=open("Productos.dat","r")
    productos=archivo.readlines()
    archivo.close()
    ventas=open("Ventas.dat","a")
    ventas.write(str(fecha) + "\t" + cliente + "\n")
    total=0
    while(True):
        codigo=input("Código del producto:")
        producto=BuscarProducto(codigo,productos)
        if(producto==None):
            print("Producto no encontrado")
        else:
            cantidad=int(input("Cantidad:"))
            importe=cantidad*float(producto[2])
            total+=importe
            ventas.write(str(cantidad) + "\t" + producto[1] + "\t" + producto[2] + "\t" + str(importe) + "\n")
            print("Total actual: $" + str(total))
        otro=input("\nOtro producto [s/n]:")
        if(otro=="n"):
            break
    ventas.write("Total: " + str(total) + "\n")
    ventas.close()
    print("Venta realizada correctamente...")
    input()
    
while(True):
    print("\tMenú de Papeleria el Compas")
    print("\n1. Menú de productos")
    print("2. Nueva venta")
    print("3. Salir")
    try:
        opcion=int(input("\tOpcion: "))
        if(opcion==1):
            MenuProductos()
        if(opcion==2):
            NuevaVenta()
        if(opcion==3):
            break
    except:
        print("Opcion incorrecta...")
