from random import randrange, choice, uniform
archivo=open("facturas.csv","w")
nombres=["Juan","Pedro","Maria","Flor","Raul","Juana"]
apellidos=["Perez","Lopez","Ramirez","Hernandez","Juarez"]
for i in range(1,32):
    importe=uniform(1000,9999)
    iva=importe*0.16
    total=importe+iva
    facura=str(i) +"/10/2021,"+ choice(nombres) + " " + choice(apellidos) + "," + str(importe) + "," + str(iva) + "," + str(total) + "\n"
    archivo.write(facura)
archivo.close()

