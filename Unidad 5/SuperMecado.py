#Crear un programa que simule un supermecado, donde cada cliente que desee comprar tiene antes que 
# adquirir una membrecia, los productos estan organizados en categorias y cada producto tiene datos de su 
#nombre, categoria (Departamento), Costo, Cantidad, Descripcion.
#Día a día, los clientes realizan compras y estas se tienen que almacenar y por la noche, sacar el reporte de ventas
#así como el inventario de productos.
# la tienda vende equipo de computo y se llama, "SuperEquipo"

listaProductos=[]
listaClientes=[]
listaVentas=[]

def NuevaVenta():
    numCliente=int(input("Número de cliente: "))
    clienteValido=None
    for cliente in listaClientes:
        if cliente["Numero"]==numCliente and cliente["EstaActivo"]:
            clienteValido=cliente
            break
    if clienteValido is not None:
        codProducto=input("Código de producto: ")
        productoValido=None
        for producto in listaProductos:
            if producto["Codigo"]==codProducto and producto["EstaActivo"]:
                productoValido=producto
                break
        if productoValido is not None:
            cantidad=int(input("Cantidad: "))
            if productoValido["Stock"]>=cantidad:
                listaVentas.append(dict(
                    Producto=productoValido,
                    Cliente=clienteValido,
                    Cantidad=cantidad,
                    Total=productoValido["Costo"]*cantidad
                ))
                print("Venta Realizada correctamente...")
            else:
                print("Cantidad insuficiente...")
        else:
            print("Producto no existente...")
    else:
        print("Cliente no existe...")




def NuevoProducto():
    listaProductos.append(dict(
        Categoria=input("Categoria: "),
        Codigo=input("Código del producto: "),
        Nombre=input("Nombre del producto: "),
        Descripcion=input("Descripción: "),
        Costo=float(input("Costo: $")),
        Stock=int(input("Stock: ")),
        EstaActivo=True
    ))
    print("Producto Agregado correctamente")

def BajaDeProducto():
    codigo=input("Código del producto a dar de baja: ")
    exito=False
    for producto in listaProductos:
        if producto["Codigo"]==codigo and producto["EstaActivo"]==True:
            producto["EstaActivo"]=False
            exito=True
    if exito:
        print("Producto eliminado correctamente")
    else:
        print("Producto no existente...")

def ModificarProducto():
    codigo=input("Código del producto a modificar: ")
    exito=False
    for producto in listaProductos:
        if producto["Codigo"]==codigo and producto["EstaActivo"]==True:
            producto["Costo"]=float(input("Costo Actual: $",producto["Costo"]," Nuevo Costo: $"))
            producto["Stock"]=float(input("Stock Actual: ",producto["Stock"]," Nuevo Stock: "))
            exito=True
            break
    if exito: 
        print("Producto modificado correctamente")
    else:
        print("Producto no existente...")

def ImprimirProductos():
    print("Código\tCategoria\tNombre\tDescripción\t\tCosto\tStock")
    i=0
    for producto in listaProductos:
        if producto["EstaActivo"]:
            print(producto["Codigo"],"\t",producto["Categoria"],"\t",producto["Nombre"],"\t",producto["Descripcion"],
            "\t\t",producto["Costo"],"\t",producto["Stock"])
            i+=1
    print("===>Productos Actuales: ",i)


def MenuProductos():
    while True:
        print("\tMenú de productos")
        print("A. Alta de producto")
        print("B. Baja de producto")
        print("C. Cambios en el producto")
        print("I. Imprimir Lista de productos")
        print("R. Regresar al menú principal")
        opcion=input("Opción: ")
        if opcion=="A":
            NuevoProducto()
        if opcion=="B":
            BajaDeProducto()
        if opcion=="C":
            ModificarProducto()
        if opcion=="I":
            ImprimirProductos()
        if opcion=="R":
            break

def NuevoCliente():
    listaClientes.append(dict(
        Numero=int(input("Número de Cliente: ")),
        Nombre=input("Nombre del cliente: "),
        EstaActivo=True
    ))
    print("Nuevo cliente creado...")

def BajaDeCliente():
    num=int(input("Número de cliente a dar de baja: "))
    exito=False
    for cliente in listaClientes:
        if cliente["Numero"]==num:
            cliente["EstaActivo"]=False
            exito=True
            break
    if exito:
        print("Cliente dado de baja correctamente")
    else:
        print("No existe cliente con ese Número...")
    
def ModificarCliente():
    num=int(input("Número de cliente a Modificar: "))
    exito=False
    for cliente in listaClientes:
        if cliente["Numero"]==num:
            cliente["Nombre"]=input("Nuevo nombre de cliente: ")
            exito=True
            break
    if exito:
        print("Cliente modifiado correctamente")
    else:
        print("No existe cliente con ese Número...")

def ImprimirClientes(todos=True):
    print("Núm.\tNombre\tActivo")
    i=0
    for cliente in listaClientes:
        if todos:
            print(cliente["Numero"],"\t",cliente["Nombre"],"\t",cliente["EstaActivo"])
            i+=1
        else:
            if cliente["EstaActivo"]:
                print(cliente["Numero"],"\t",cliente["Nombre"])
                i+=1
    print("===> Clientes totales: ",i)


def MenuClientes():
    while True:
        print("\tMenú de clientes")
        print("A. Alta de cliente")
        print("B. Baja de cliente")
        print("C. Cambios en el cliente")
        print("I. Imprimir Lista de Clientes")
        print("R. Regresar al menú principal")
        opcion=input("Opción: ")
        if opcion=="A":
            NuevoCliente()
        if opcion=="B":
            BajaDeCliente()
        if opcion=="C":
            ModificarCliente()
        if opcion=="I":
            ImprimirClientes()
        if opcion=="R":
            break

def Menu():
    print("\tSuperEquipo")
    print("C. Menú de Clientes")
    print("P. Menú de Productos")
    print("R. Reportes")
    print("V. Nueva Venta")
    print("S. Salir")
    return input("Opción: ")

while True:
    opcion=Menu()
    if opcion=="C":
        MenuClientes()          
    if opcion=="P":
        MenuProductos()
    if opcion=="V":
        NuevaVenta()
    #if opcion=="R":
    #    MenuReportes()
    if opcion=="S":
        print("Programa terminado!!!")
        break
    