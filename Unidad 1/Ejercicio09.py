print("Menú")
print("1. Radianes a Grados, Minutos y Segundos")
print("2. Grados, Minutos y Segundos a Radianes")
opcion=input("Opción:")
if(opcion=="1"):
    print("===De radianes a grados===")
    rad=int(input("Dame el valor en radianes:"))
    temp=rad*180/3.1416
    grad=temp//1 #truncamos el resultado
    residuo=temp-grad
    min=(residuo*60)//1
    residuo2=(residuo*60)-min
    seg=residuo2*60
    print("El resultado es:",grad,"°",min,"'",seg,"''")
else:
    print("===De grados a radianes===")
    #temp=grad+(min/60)+(seg/3600)
    #Rad=temp*(3.1416/180)
    grad=int(input("Dame los grados:"))
    min=int(input("Dame los minutos:"))
    seg=float(input("Dame los segundos:"))
    temp=grad+(min/60)+(seg/3600)
    rad=temp*(3.1416/180)
    print("El resultado en radianes es:", rad)
print("Gracias por usar este programa...")   
