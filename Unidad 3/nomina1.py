trabajadores=int(input("Total de trabajadores a calcular:"))
i=0
granTotal=0
bonos=0
b500=0
b200=0
b100=0
b50=0
b20=0
m10=0
m5=0
m2=0
m1=0
while i<trabajadores:
    print("\n\tTrabajador",i+1,":")
    horasTrabajadas=float(input("Dame el total de horas trabajadas:"))
    costoPorHora=float(input("Costo por hora:$"))
    total=horasTrabajadas*costoPorHora
    if horasTrabajadas>35:
        total=total*1.1
        bonos+=1
    print("El salario total es de:$",total)
    i+=1
    granTotal+=total   #granTotal=granTotal+total
    
    tb500=total//500
    b500+=tb500
    total=total-(tb500*500)
    
    tb200=total//200
    b200+=tb200
    total=total-(tb200*200)

    tb100=total//100
    b100+=tb100
    total=total-(tb100*100)

    tb50=total//50
    b50+=tb50
    total=total-(tb50*50)

    tb20=total//20
    b20+=tb20
    total=total-(tb20*20)

    tm10=total//10
    m10+=tm10
    total=total-(tm10*10)

    tm5=total//5
    m5+=tm5
    total=total-(tm5*5)

    tm2=total//2
    m2+=tm2
    total=total-(tm2*2)

    tm1=total//1
    m1+=tm1
    total=total-(tm1)

    if(total>0):
        tm1+=1
        m1+=1
    
    print("500:",tb500,"200:",tb200,"100:",tb100,"50:",tb50,"20:",tb20,"10:",tm10,"5:",tm5,"2:",tm2,"1:",tm1)

print("El total de la nomina es:$", granTotal)
print("Trabajadores con Bono:", bonos)
noBonos=trabajadores-bonos
# trab -> 100%
# noBonos -> ? %
print("Trabajadores sin bono:",noBonos*100/trabajadores,"%")
print("500:",b500,"200:",b200,"100:",b100,"50:",b50,"20:",b20,"10:",m10,"5:",m5,"2:",m2,"1:",m1)
print("Programa terminado...")
